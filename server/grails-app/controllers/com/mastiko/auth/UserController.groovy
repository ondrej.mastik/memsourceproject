package com.mastiko.auth

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.*

@Secured('ROLE_ADMIN')
class UserController {
    UserService userService
    SpringSecurityService springSecurityService

    static responseFormats = ['json']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond userService.list(params), model: [userCount: userService.count()]
    }

    @Secured('isAuthenticated()')
    def show(Long id) {
        def authenticatedUser = User.findByUsername(springSecurityService.principal.username)
        if (authenticatedUser.id != id
                && !authenticatedUser.getAuthorities().contains(new Role(authority: 'ROLE_ADMIN'))) {
            render status: FORBIDDEN
            return
        }
        respond userService.get(id)
    }

    @Secured('IS_AUTHENTICATED_ANONYMOUSLY')
    def save(User user) {
        if (user == null) {
            render status: NOT_FOUND
            return
        }

        try {
            userService.save(user)
        } catch (ValidationException e) {
            respond user.errors
            return
        }

        respond user, [status: CREATED]
    }

    @Secured('isAuthenticated()')
    def update(User user) {
        if (user == null) {
            render status: NOT_FOUND
            return
        }

        def authenticatedUser = User.findByUsername(springSecurityService.principal.username)
        user.id = authenticatedUser.id

        try {
            userService.save(user)
        } catch (ValidationException e) {
            respond user.errors
            return
        }

        respond user, [status: OK]
    }

    def delete(Long id) {
        if (id == null) {
            render status: NOT_FOUND
            return
        }

        userService.delete(id)

        render status: NO_CONTENT
    }
}
