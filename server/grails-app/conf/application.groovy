grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.mastiko.auth.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.mastiko.auth.UserRole'
grails.plugin.springsecurity.authority.className = 'com.mastiko.auth.Role'

grails.plugin.springsecurity.filterChain.chainMap = [
		[ pattern: '/api/**', filters: 'JOINED_FILTERS,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'],
]
