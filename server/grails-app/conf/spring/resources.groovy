import com.mastiko.auth.CustomAppRestAuthTokenJsonRenderer
import com.mastiko.auth.UserPasswordEncoderListener
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint

// Place your Spring DSL code here
beans = {
    userPasswordEncoderListener(UserPasswordEncoderListener)
    authenticationEntryPoint(Http403ForbiddenEntryPoint)
    accessTokenJsonRenderer(CustomAppRestAuthTokenJsonRenderer)
}
