import authHeader from './auth-header'
import user from './user'

const INTERNAL_API_URL = 'http://localhost:8080/api/'

class AuthService {
  login (credentials, onSuccess, onError) {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    }
    return fetch(INTERNAL_API_URL + 'login', requestOptions)
      .then(response => {
        if (response.status === 200) {
          response.json().then((responseData) => {
            localStorage.setItem('user', JSON.stringify(responseData))
            onSuccess()
          })
        } else {
          onError()
        }
      })
  }

  logout () {
    localStorage.removeItem('memsource-api-token')
    localStorage.removeItem('user')
  }

  memsourceApiLogin (credentials, onSuccess) {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    }
    fetch('https://cloud.memsource.com/web/api2/v1/auth/login', requestOptions)
      .then(response => {
        if (response.status === 200) {
          response.json().then(responseData => {
            localStorage.setItem('memsource-api-token', responseData.token)
            onSuccess(responseData)
          })
        } else {
          alert('Invalid credentials for memsource api')
        }
      })
  }

  editUser (userData, onSuccess, onError) {
    const requestOptions = {
      method: 'PUT',
      headers: {
        ...authHeader(),
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(userData)
    }
    fetch(INTERNAL_API_URL + 'user/' + user().id, requestOptions)
      .then(response => {
        if (response.status === 200) {
          localStorage.removeItem('memsource-api-token')
          onSuccess()
        } else {
          onError()
          alert('Something went wrong')
        }
      })
  }
  register (userData, onSuccess, onError) {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(userData)
    }
    fetch(INTERNAL_API_URL + 'user', requestOptions)
      .then(response => {
        if (response.status === 201) {
          onSuccess()
        } else {
          onError()
          alert('Something went wrong')
        }
      })
  }
}

export default new AuthService()
