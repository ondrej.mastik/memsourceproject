export default function user () {
  return JSON.parse(localStorage.getItem('user')) || null
}
