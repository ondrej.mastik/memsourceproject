import authHeader from './auth-header'
import user from './user'

export default function withMemsourceCredentials (action, onSuccess) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  }
  let userData = user()
  return fetch('http://localhost:8080/api/user/' + userData.id, requestOptions)
    .then(response => {
      if (response.status === 200) {
        response.json().then(responseData => {
          console.log(responseData)
          action({
            userName: responseData.memsourceApiUser,
            password: responseData.memsourceApiPassword
          }, onSuccess)
        })
      } else {
        alert('Error')
      }
    })
}
