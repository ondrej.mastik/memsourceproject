import user from './user'

export default function authHeader () {
  let userData = user()

  if (userData && userData.access_token) {
    return { Authorization: 'Bearer ' + userData.access_token }
  } else {
    return {}
  }
}
