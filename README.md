# MemsourceProject

Java / Groovy Developer Assignment repo

To run this application, in separate command line instances go to project root directory and type

gradlew server:bootRun

gradlew client:start

after client starts, see output in console for frontend url (for example "Your application is running here: http://localhost:3001"
)

Please note that it's not possible to create your own user using frontend, so far - use username "user" and password "user" for login.
